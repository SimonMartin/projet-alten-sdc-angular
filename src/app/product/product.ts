export interface Product {
    id: number;
    code: string;
    name: string;
    description: string;
    image: string;
    category: string;
    inventoryStatus: InventoryStatus;
    price: number;
    quantity: number;
    rating: number;
}

export enum Category {
    Accessories = "Accessories",
    Clothing = "Clothing",
    Electronics = "Electronics",
    Fitness = "Fitness",
}

export enum InventoryStatus {
    Instock = "INSTOCK",
    Lowstock = "LOWSTOCK",
    Outofstock = "OUTOFSTOCK",
}