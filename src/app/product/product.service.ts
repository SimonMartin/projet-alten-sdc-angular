import { Injectable } from '@angular/core';

import { InventoryStatus, Product } from './product';

@Injectable({
  providedIn: 'root'
})

export class ProductService {
  private readonly dbUrl = 'http://localhost:3000/data';

  constructor() { }

  async getAllProducts(): Promise<Product[]> {
    const data = await fetch(this.dbUrl);
    return await data.json() ?? [];
  }

  async getProductById(id: number): Promise<Product | undefined> {
    const data = await fetch(`${this.dbUrl}/${id}`);
    return await data.json() ?? [];
  }

  async setProductById(id: number, product: Product) {
    await fetch(this.dbUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(product)
    })
  }
}

export const defaultProduct = {
  id: -1,
  code: "",
  name: "",
  description: "",
  image: "",
  price: -1,
  category: "",
  quantity: -1,
  inventoryStatus: InventoryStatus.Outofstock,
  rating: -1
}
