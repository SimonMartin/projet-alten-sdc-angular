import { Component, inject } from '@angular/core';

import { ProductService } from '../product.service';
import { PrimeNGModule } from '../../utils/primeng/primeng.module';
import { Product } from '../product';

@Component({standalone: true,
  selector: 'app-shop-table',
  imports: [
    PrimeNGModule
  ],
  templateUrl: './shop-table.component.html',
  styleUrl: './shop-table.component.scss'
})

export class ShopTableComponent {
  
  productService: ProductService = inject(ProductService);
  products: Product[] = [];
  filteredProducts: Product[] = [];

  constructor() {
    this.productService.getAllProducts().then(products => {
      this.products = products;
    });
  }
}
