import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductService, defaultProduct } from '../product.service';
import { PrimeNGModule } from '../../utils/primeng/primeng.module';
import { Product } from '../product';

@Component({
  standalone: true,
  selector: 'app-admin-table',
  imports: [
    CommonModule,
    PrimeNGModule
  ],
  templateUrl: './admin-table.component.html',
  styleUrl: './admin-table.component.scss'
})

export class AdminTableComponent implements OnInit {
  productService: ProductService = inject(ProductService);

  products: Product[] = [];
  filteredProducts: Product[] = [];

  product!: Product;
  selectedProducts: Product[] = [];;

  productDialog: boolean = false;
  submitted: boolean = false;

  get deleteDisabled() {
    return this.selectedProducts?.length < 1;
  }

  constructor() {
    this.productService.getAllProducts().then(products => {
      this.products = products;
      this.filteredProducts = products;
    });
  }

  ngOnInit() {
    this.productService.getAllProducts().then((data) => {
      this.products = data;
      this.filteredProducts = data;
    });
    
    this.product = defaultProduct;
}

  openNew() {
    this.product = defaultProduct;
    this.submitted = false;
    this.productDialog = true;
    console.log('TOP')
  }

  hideDialog() {
    this.productDialog = false;
    this.submitted = false;
  }

  filterByCode(input: string) {
    console.log('FILTER BY CODE');
    if (!input) {
      this.filteredProducts = this.products;
    }
    this.filteredProducts = this.products?.filter(product => product.code.toLowerCase().includes(input.toLowerCase()));
  }

  filterByName(input: string) {
    console.log('FILTER BY CODE');
    if (!input) {
      this.filteredProducts = this.products;
    }
    this.filteredProducts = this.products?.filter(product => product.name.toLowerCase().includes(input.toLowerCase()));
  }

  editProduct(product: Product) {

  }

  deleteProduct(product: Product) {

  }

  saveProduct() {
    this.submitted = true;

    if (this.product.name?.trim()) {
      if (this.product.id === -1) {
        this.productService.setProductById(this.product.id, this.product);
      } else {
        this.product.id = 1000 + this.products.length;
        this.productService.setProductById(this.product.id, this.product);
        this.product.image = 'product-placeholder.svg';
      }

      this.products = [...this.products];
      this.productDialog = false;
      this.product = defaultProduct;
    }
  }
}
