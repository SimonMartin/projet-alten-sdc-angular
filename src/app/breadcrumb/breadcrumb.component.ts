import { Component, Input, OnInit, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SidenavItem } from '../sidenav/sidenav.model';
import { SidenavService } from '../sidenav/sidenav.service';
import { SIDENAV_ITEMS } from '../sidenav/SIDENAV_ITEMS';
import { MenuItem } from 'primeng/api';
import { filter, map, startWith, tap } from 'rxjs';
import { PrimeNGModule } from '../utils/primeng/primeng.module';

@Component({
  standalone: true,
  imports: [
    PrimeNGModule,
  ],
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})

export class BreadcrumbComponent implements OnInit {
  private readonly route: ActivatedRoute = inject(ActivatedRoute);

  @Input() public lang = 'en';
  public items: MenuItem[] = [];
  private readonly sidenavItems: SidenavItem[] = SIDENAV_ITEMS;
  private homeItem: MenuItem = { label: 'Home', routerLink: '/' };
  

  constructor(
    private readonly sidenavService: SidenavService,
  ) {

  }

  ngOnInit(): void {
    console.log('ROUTE: ', this.route.snapshot);
    this.buildBreadcrumb(this.route.snapshot.params['id']);
  }

  private buildBreadcrumb(path: string): void {
    const firstPath: SidenavItem | undefined = this.sidenavItems.find(item => '/' + item.id === path);
    console.log('FIRST PATH', firstPath)
    if (firstPath) {
      this.items.push({
        label: firstPath.labels[this.lang],
        routerLink: firstPath.link,
        command: () => this.sidenavService.setCurrentEntityName('')
      });
    }
  }
}