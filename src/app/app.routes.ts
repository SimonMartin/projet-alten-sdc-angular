import { Routes } from '@angular/router';
import { AdminTableComponent } from './product/admin-table/admin-table.component';
import { ShopTableComponent } from './product/shop-table/shop-table.component';

const routes: Routes = [
    {
        path: 'admin/products',
        component: AdminTableComponent,
        title: 'Admin Products',
    },
    {
        path: 'products',
        component: ShopTableComponent,
        title: 'Admin Products',
    }
];

export default routes;