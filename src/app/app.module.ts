import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { provideRouter } from '@angular/router';
import localeEn from '@angular/common/locales/en';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import routes from './app.routes';

import { MainContentComponent } from './main-content/main-content.component';
import { ProductModule } from './product/product.module';
import { SidenavComponent } from './sidenav/sidenav.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ProductModule,
    SidenavComponent,
    BreadcrumbComponent,
    MainContentComponent,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'en' },
    provideRouter(routes),
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor() {
    registerLocaleData(localeEn, 'en');
  }
}
