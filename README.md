# Projet Alten SDC Angular

## Explications
J'ai rencontré des difficultés utiliser le projet de base, notamment lors de l'installation des modules et de la compilation. J'ai essayé avec différentes versions de Node, et j'ai essayer de corriger les problèmes à la main.

J'ai décidé de repartir depuis une application 'from-scratch' en reprennant certains composants du projet initial.

## Avancement
L'application a la page Admin qui est visuellement complète et pour la partie fonctionnalité, la pagination, le filtrage,  le sort et l'ajout d'un nouveau produit (sommaire) est fonctionnel.

J'utilise pour la partie "back" json-server.
